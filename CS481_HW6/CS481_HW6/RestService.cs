﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CS481_HW6
{
    class RestService
    {
        HttpClient _client;

        public RestService()
        {
            _client = new HttpClient();
        }

        //
        public async Task<Owlbot.Dictionary> GetDictionaryDataAsync(string uri)
        {
            Owlbot.Dictionary dictionaryData = null;
            //tries to access dictionary with access API code and user-entered word
            try
            {
                HttpResponseMessage response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    dictionaryData = JsonConvert.DeserializeObject<Owlbot.Dictionary>(content);
                    return dictionaryData;
                }
            }
            //access failed, writes exception
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return dictionaryData;
        }
    }
}
