﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin.Forms;

namespace CS481_HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        RestService restService;

        public MainPage()
        {
            InitializeComponent();
            restService = new RestService();
        }

        //function for when define bvutton is clicked
        async void GetDefinition_Clicked(object sender, System.EventArgs e)
        { 
            //checks if string has content
            if (!string.IsNullOrWhiteSpace(enteredWord.Text))
            {
                //grabs variables from owlbot and stores data into dictionaryData
                Owlbot.Dictionary dictionaryData = await restService.GetDictionaryDataAsync(GenerateRequestUri(Constants.DictionaryEndpoint));
                BindingContext = dictionaryData;
            }
        }

        //generates the link to obtain the definition data
        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"?q={enteredWord.Text}";
            requestUri += "&units=imperial"; // or units=metric
            requestUri += $"&APPID={Constants.DictionaryAPIKey}";
            return requestUri;
        }
    }
}
